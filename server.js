var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');
var requestjson = require('request-json');
var bodyparser = require('body-parser');
var movimientosJSON = require('./movimientosv2.json');
var urlMlabRaiz = 'https://api.mlab.com/api/1/databases/omartineza/collections/';
var urlClientes = 'Clientes?';
var apiKey = 'apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt';
var clienteMlab = requestjson.createClient(urlMlabRaiz+urlClientes+apiKey);

app.use(bodyparser.json());
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});
app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get('/', function(req,res) {
  res.sendFile(path.join(__dirname, 'index.html'));
});

app.post('/', function(req,res) {
  res.send('Hemos recibido su petición post');
});

app.put('/', function(req,res) {
  res.send('Hemos recibido su petición PUT cambiada');
});

app.delete('/', function(req,res) {
  res.send('Hemos recibido su petición DELETE');
});

app.get('/Clientes/:idcliente', function(req, res) {
  res.send('Aquí tiene el cliente '+req.params.idcliente);
});

app.get('/v1/Movimientos', function(req, res) {
  res.sendfile('movimientosv1.json');
});

app.get('/v2/Movimientos', function(req, res) {
  res.json(movimientosJSON);
});

app.get('/v2/Movimientos/:idcliente', function(req, res) {
  res.send(movimientosJSON[req.params.idcliente-1]);
});

app.get('/v2/Movimientosquery', function(req, res) {
  res.send(req.query);
});

app.post('/v2/Movimientos', function(req, res) {
  let nuevo = req.body;
  nuevo.id = movimientosJSON.length + 1;
  movimientosJSON.push(nuevo);
  res.send("Movimiento dado de alta");
});

app.get('/Clientes', function(req, res) {
  clienteMlab.get('', function(err, resM, body) {
    if(!err) {
      res.send(body);
    } else {
      console.log(body);
    }
  });
});

app.post('/Clientes', function(req,res) {
  clienteMlab.post('', req.body, function(err, resM, body) {
    res.send(body);
  });
});

app.post('/Login', function(req, res) {
  var email = req.body.email;
  var password = req.body.password;
  var query = 'q={"email":"'+email+'", "password":"'+password+'"}';

  console.log(query);
  var mlabClient = requestjson.createClient(urlMlabRaiz+'Usuarios?'+apiKey+'&'+query);
  console.log(urlMlabRaiz+'Usuarios?'+apiKey+'&'+query);
  mlabClient.get('', function(err, resM, body) {
    if(!err) {
      if(body.length==1) {
        res.status(200).send("Usuario logado");
      } else {
        res.status(400).send("Usuario no encontrado");
      }
    }
  });
});
